<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:param name="api-key" select="''"/>
    <xsl:param name="input-file" select="'../ISSNlastyears.txt'"/>
    <xsl:param name="debug" as="xs:boolean" select="false()" static="1"/>
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:variable name="issns" select="distinct-values(unparsed-text-lines($input-file)[matches(.,'(ISSN|eISSN)?\s?[\S]{4}\-[\S]{4}')])" as="xs:string*"/>
        <xsl:variable name="issns-label" select="'ISSNs'"/>
        <xsl:message select="'processing ' || count($issns) || ' '||$issns-label"/>

        <xsl:for-each select="$issns">
            <xsl:variable name="issn" select="."/>
            <!-- lage individuelt api kall-->
            <xsl:variable name="request-uri" expand-text="1">https://v2.sherpa.ac.uk/cgi/retrieve_by_id?item-type=publication&amp;api-key={$api-key}&amp;format=Json&amp;identifier={$issn}</xsl:variable>       
            <xsl:variable name="api-result" select="json-to-xml(unparsed-text($request-uri))"/>
            <xsl:variable name="pos" as="xs:integer" select="position()"/>
            
            <xsl:if test="$debug">
            <xsl:message select="serialize($api-result)"/>
            </xsl:if>
            <!-- do something with the result -->
            <xsl:apply-templates select="$api-result" mode="published_issn">
                <xsl:with-param name="issn" select="$issn" tunnel="yes"/>
            </xsl:apply-templates> 
            
            <xsl:if test="$pos mod 100 = 0 ">
                <xsl:message select="'processed ' || $pos || ' ' || $issns-label"/>
            </xsl:if> 
        </xsl:for-each>
        
        <xsl:message select="'done'"/>
    </xsl:template>
    
    <xsl:template match="*:array[@key = 'items']" mode="published_issn">
        <xsl:param name="issn" tunnel="yes" as="xs:string"/>
        <xsl:variable name="allowed_versions" select="'published'"/>
        <xsl:variable name="allowed_locations"
            select="'any_website', 'non_commercial_repository','non_commercial_institutional_repository', 'any_repository', 'institutional_repository'"/>
        <xsl:variable name="permitted_oa_items"
            select="descendant::*:array[@key = 'permitted_oa']/*:map" as="element(*)*"/>
        <xsl:variable name="embargo-limit" select="2" as="xs:integer"/>        
        
        
        <!-- checks for location institutional_repository and 'published' in same policy -->
        <xsl:variable name="allows_inst_repo_published"
            select="
            some $policy in $permitted_oa_items
            satisfies $policy/descendant::*:array[@key = 'location']/*:string[. = $allowed_locations]
            and $policy/descendant::*:array[@key = 'article_version']/*:string[. = $allowed_versions]
            and $policy/descendant::*:string[@key = 'additional_oa_fee'][.='no']
            and not(xs:integer($policy/descendant::*:map[@key='embargo'][*:string[@key='units']='years']/*:number[@key='amount']) > $embargo-limit)"/>
        
        <xsl:text expand-text="1">{$issn},{$allows_inst_repo_published}
</xsl:text>
       
    </xsl:template>
    
</xsl:stylesheet>