# Repo for helper scripts for Publiseringsstøtte


For fetching data and doing tasks that could end up being repeatable.
Only one script so far

Currently has:

# Query Romeo for ISSNs that can be deposited and stored
1. Update ISSNlastyears.txt with ISSNs, one per line
2. Download artifacts when CI job finishes

* Rules created by Marta and Tormod.
* API key is available as CI variable in this repo, if Romeo access is needed for something else.



